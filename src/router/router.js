const express = require('express');
const router = express.Router();
const { dayfinderController } = require('../controllers/dayfinder');
const { historiqueController } = require('../controllers/historique');

router.post('/', dayfinderController);
router.get('/historique', historiqueController);

module.exports = router;
