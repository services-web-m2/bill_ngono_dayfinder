const dayjs = require('dayjs');
const localeFr = require('dayjs/locale/fr');
const customParseFormat = require('dayjs/plugin/customParseFormat');
dayjs.extend(customParseFormat);
dayjs.locale('fr');
const mysql = require('mysql2/promise');
const pool = require("../database")


function convertDateFormat(date) {
  if (date.includes('/')) {
    return date.split('/').join('-');
  }
  return date;
}

exports.dayfinderController = async (req, res) => {
  const inputDate = convertDateFormat(req.body.date);
  const date = dayjs(inputDate, 'DD-MM-YYYY');

  if (!date.isValid()) {
    res.status(400).json({ error: "Format de date invalide. Les formats acceptés sont 'JJ/MM/AAAA' ou 'JJ-MM-AAAA'." });
    return;
  }
  
  const dayOfWeek = date.format('dddd');
  const searchDate = dayjs().format('YYYY-MM-DD HH:mm:ss');

  const searchItems = {
    request: req.body.date,
    response: {
      date: inputDate,
      dayOfWeek: dayOfWeek
    }
  };

  // Enregistre la date dans la base de données MySQL
  const [rows, fields] = await pool.execute(
    'INSERT INTO history (searchDate, request, response) VALUES (?, ?, ?)',
    [searchDate, req.body.date, JSON.stringify(searchItems)]
  );

  res.json({ date: inputDate, dayOfWeek });
};
