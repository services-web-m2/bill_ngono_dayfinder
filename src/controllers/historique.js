const mysql = require('mysql2/promise');

const pool = require("../database")

exports.historiqueController = async (req, res) => {
  // Récupère l'historique de la base de données MySQL
  const [rows, fields] = await pool.execute('SELECT * FROM history');
  
  // Convertir le champ `response` en objet JSON
  rows.map(row => {
    if (typeof row.response === 'string') {
      row.response = JSON.parse(row.response);
    }
  });
  

  res.json(rows);
};
