const express = require('express');
const bodyParser = require('body-parser');
const dayFinderRouter = require('./router/router');
const pool = require('./database');

const app = express();

app.use(bodyParser.json());

const checkAndCreateTable = async () => {
  try {
    await pool.execute(
      `CREATE TABLE IF NOT EXISTS history (
        id INT AUTO_INCREMENT,
        searchDate DATETIME,
        request VARCHAR(255),
        response JSON,
        PRIMARY KEY (id)
      )`
    );
  } catch (err) {
    console.error("Error creating table: ", err);
  }
};

// Vérifie et crée la table lors du démarrage de l'application
checkAndCreateTable();

app.use('/dayfinder', dayFinderRouter);

app.listen(8080, () => {
  console.log('App running on port 8080.');
});
